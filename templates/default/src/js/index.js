window.$ = window.jQuery = require('jquery');

document.addEventListener('DOMContentLoaded', () => {
	require('./js-polyfills');
	require('lazysizes');
	require('jquery-match-height');
	require('./plugins/tabs/js/tabs');
	require('./swiper');
	require('./swiper-large');
	require('./swiper-vertical');
	require('./swiper-detail');
	require('./plugins/Popup/js/popup');
	window.Inputmask = require('inputmask');
	require('./app');
	require('./form');
	window.noUiSlider = require('nouislider');
	require('./filter');
	require('./scroll-up');
	require('./scrolling-section');
	require('./compare');
});