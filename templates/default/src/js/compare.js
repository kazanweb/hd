class Compare {

	constructor() {
		this.proxyRows = []
	}

	sectionHide(buttons) {

		buttons.forEach(function (node) {
			node.addEventListener('click', function () {
				this.parentNode.classList.toggle('active');
			});
		});

	}

	compareBlocks(trigger, row, selectorProperties) {

		if (trigger) {
			if (this.proxyRows.length == 0) {
				row.forEach((node) => {

					let props = node.querySelectorAll(selectorProperties);
					let propsFirstItemText = props[0].innerHTML;
					let trigger = false;

					for (let i = 1; i < props.length; i++) {
						if (props[i].innerHTML != propsFirstItemText) {
							trigger = true;
							break;
						}
					}

					if (trigger) {
						this.proxyRows.push(node);
						node.classList.add('hide');
					}
				});
			} else {
				this.proxyRows.forEach(function (node) {
					node.classList.add('hide');
				});
			}

		} else {
			row.forEach(function (node) {
				node.classList.remove('hide');
			});
		}

	}

	arrowNavigation(prev, next, scrollItems) {

		let mq = window.matchMedia('(max-width: 768px)');
		const distance = mq.matches ? 148 : 278;
		let i = 0;

		prev.addEventListener('click', function () {
			i = i + distance;
			scrollItems.forEach(function (node) {
				$(node).stop().animate({ scrollLeft: '-=' + distance });
			});
		});

		next.addEventListener('click', function () {
			i = i - distance;
			scrollItems.forEach(function (node) {
				$(node).stop().animate({ scrollLeft: '+=' + distance });
			});
		});
	}

	scroll(scrollItems) {

		scrollItems.forEach(function (item) {
			item.addEventListener('scroll', function () {
				var obj = this;
				scrollItems.forEach(function (node) {
					if (node !== obj) {
						node.scrollLeft = obj.scrollLeft;
					}
				});

			});
		});

	}
}

if (document.querySelector('#js-compare')) {

	let scrollItems = document.querySelectorAll('[data-compare-scroll]');
	let prev = document.querySelector('[data-compare-prev]');
	let next = document.querySelector('[data-compare-next]');
	let checkbox = document.querySelector('[data-compare-checkbox]');
	let row = document.querySelectorAll('[data-compare-row]');
	let buttons = document.querySelectorAll('[data-compare-button]');

	let compare = new Compare();

	compare.scroll(scrollItems);
	compare.arrowNavigation(prev, next, scrollItems);
	compare.sectionHide(buttons);
	compare.compareBlocks(true, row, '[data-compare-prop]');

	checkbox.addEventListener('change', function () {
		if (this.checked) {
			compare.compareBlocks(true, row, '[data-compare-prop]');
		} else {
			compare.compareBlocks(false, row, '[data-compare-prop]');
		}
	});
}
