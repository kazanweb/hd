(function () {

	var $filter = document.querySelector('#js-filter');
	var $filterCheckboxForChangeForm;
	var $filterForm;
	var $filterHiddenBlock;
	var $filterButtonMore;
	var $filterBrands;
	var $filterBrandsBtnShow;
	var $filterSlider;

	var $filterPopups;

	if (!$filter) {
		return false;
	}

	$filterForm = $filter.querySelector('form');
	$filterCheckboxForChangeForm = $filter.querySelector('#js-filter-hidden');
	$filterHiddenBlock = $filter.querySelector('[data-filter-hidden]');
	$filterButtonMore = $filter.querySelector('[data-filter-more]');
	$filterBrands = $filter.querySelector('[data-filter-brands]');
	$filterBrandsBtnShow = $filter.querySelector('[data-filter-brands-btn]');

	$filterButtonMore.addEventListener('click', function () {
		this.classList.toggle('active');
		$filterHiddenBlock.classList.toggle('active');
	});

	if ($filterBrandsBtnShow) {
		$filterBrandsBtnShow.addEventListener('click', function () {
			$filterBrands.classList.toggle('active');
		});
	}

	// вызов попапов
	$filterPopups = [...document.querySelectorAll('.enable[data-filter-popup]')];
	$filterPopups.forEach(function (node) {

		var $filterPopupsCall = node.querySelectorAll('[data-filter-popup-call]');
		var $filterPopupsClose = node.querySelector('[data-filter-popup-close]');
		var $filterPopupsValue = node.querySelector('[data-filter-value]');
		var $inputsCheckbox;

		node.addEventListener('click', function (e) {
			e.stopPropagation();
		});

		$filterPopupsCall.forEach(function (nodeCall) {
			nodeCall.addEventListener('click', function (e) {
				e.stopPropagation();
				$filterPopups.forEach((p) => {
					if (this.parentNode != p) {
						p.classList.remove('active');
					}
				});
				node.classList.toggle('active');
			});
		});

		$filterPopupsClose.addEventListener('click', function (e) {
			e.stopPropagation();
			node.classList.remove('active');
		});

		if ($filterPopupsValue) {
			$inputsCheckbox = [...node.querySelectorAll('input[type=checkbox]')];
			if ($inputsCheckbox.length) {
				$inputsCheckbox.forEach((input) => {
					input.addEventListener('change', function () {
						let checkArray = [];
						$inputsCheckbox.forEach((item) => {
							if (item.checked) {
								checkArray.push(item.getAttribute('data-text'));
							}
						});

						$filterPopupsValue.innerHTML = checkArray.length ? checkArray.join(',') : 'Любая';

					});
				});
			}
		}

	});

	$filterSlider = [...$filter.querySelectorAll('[data-filter-slider]')];

	// инициализация слайдеров
	import('nouislider').then((noUiSlider) => {

		$filterSlider.forEach(function (node) {

			var $noUiSlider = node.querySelector('[data-nouislider]');

			if (node.classList.contains('noUi-target')) {
				return false;
			}

			node.addEventListener('click', function (e) {
				e.stopPropagation();
			});

			var $inputs = [...node.querySelectorAll('input')];
			var $result = node.querySelector('[data-filter-value]');
			var valStart = parseInt($noUiSlider.getAttribute('data-value-start'));
			var valEnd = parseInt($noUiSlider.getAttribute('data-value-end'));
			var resStart = valStart;
			var resEnd = valEnd;
			var min = parseInt($noUiSlider.getAttribute('data-min'));
			var max = parseInt($noUiSlider.getAttribute('data-max'));

			$inputs.forEach(function (input, index) {

				input.addEventListener('blur', function () {

					if (index == 0) {

						if (parseInt(this.value) >= parseInt($inputs[1].value)) {
							this.value = $inputs[1].value;
						}

						if (parseInt(this.value) <= 0) {
							this.value = 0;
						}

						$noUiSlider.noUiSlider.set([this.value, null]);

						resStart = this.value;

						if ($result) {
							$result.innerHTML = (resStart == min && resEnd == max) ? 'Любая' : `<b>${resStart}-${resEnd} см</b>`;
						}

						return false;

					}

					if (parseInt(this.value) <= parseInt($inputs[0].value)) {
						this.value = $inputs[0].value;
					}

					if (parseInt(this.value) > max) {
						this.value = max;
					}

					resEnd = this.value;

					$noUiSlider.noUiSlider.set([null, this.value]);

					if ($result) {
						$result.innerHTML = (resStart == min && resEnd == max) ? 'Любая' : `<b>${resStart}-${resEnd} см</b>`;
					}

				});

			});

			noUiSlider.create($noUiSlider, {
				start: [
					valStart,
					valEnd
				],
				connect: true,
				range: {
					'min': min,
					'max': max
				}
			});

			$noUiSlider.noUiSlider.on('slide', function (values) {
				resStart = Math.round(values[0]);
				resEnd = Math.round(values[1]);
				$inputs[0].value = resStart;
				$inputs[1].value = resEnd;
				if ($result) {
					$result.innerHTML = `<b>${resStart}-${resEnd} см</b>`
				}
			});

			$noUiSlider.noUiSlider.on('end', function (values) {
				if (resStart == min && resEnd == max) {
					if ($result) {
						$result.innerHTML = 'Любая'
					}
				}
				$filterCheckboxForChangeForm.click();
				$noUiSlider.closest('[data-filter-popup]').classList.add('active');
			});

		});
	});

	// удаление активных классов в фильтре и скрытие попапов
	document.addEventListener('click', function () {
		$filterPopups.forEach(function (node) {
			node.classList.remove('active');
		});
	});

	// раскрытие фильтра в мобилке
	[...document.querySelectorAll('[data-toggle-filter]')].forEach(function (node) {
		node.addEventListener('click', function () {
			this.classList.toggle('active');
			node.parentNode.parentNode.classList.toggle('active');
		})
	});

	$filterForm.addEventListener('change', function () {
		console.log('change form');
	})

})()