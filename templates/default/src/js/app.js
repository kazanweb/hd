window.globalPopup = new Popup();

(function () {

	! function () {
		var t = function (t) {
			this.opts = this.extendFn({
				time: 0,
				callback: function () { },
				timeEnd: function () { }
			}, t), this.init()
		};
		t.prototype = {
			init: function () {
				var t, n, e, o, i, a = this,
					r = Date.parse(this.opts.time),
					s = setInterval(function () {
						t = r - Date.parse(new Date), n = Math.floor(t / 1e3 % 60), e = Math.floor(t / 1e3 / 60 % 60), o = Math.floor(t / 36e5 % 24), i = Math.floor(t / 864e5), n < 0 && (n = 0, e = 0, o = 0, i = 0, clearInterval(s), a.opts.timeEnd(i, o, e, n)), a.opts.callback(i, o, e, n)
					}, 1e3)
			},
			extendFn: function (t, n) {
				var e;
				for (e in n) n.hasOwnProperty(e) && (t[e] = n[e]);
				return t
			}
		}, window.TimeCounter = t
	}();

	var timers = document.querySelectorAll('[data-time]');

	timers.forEach(function (node) {

		new TimeCounter({
			time: node.getAttribute('data-time'),
			callback: function (day, hour, minutes, seconds) {
				node.innerHTML = `<span>${day}</span> д <i>:</i> <span>${hour}</span> ч <i>:</i>  <span>${minutes}</span> мин <i>:</i>  <span>${seconds}</span> сек`;
			},
			timeEnd: function (day, hour, minutes, seconds) {
				alert('End')
			}
		});

	});

	(function () {

		if (document.querySelector('[data-fancybox]')) {
			if (!window.fancybox) {
				var script = document.createElement('script');
				var link = document.createElement('link');
				script.src = 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js';
				link.rel = 'stylesheet';
				link.href = 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css';
				document.body.appendChild(link);
				document.body.appendChild(script);
			}
		}

	})()

	$('[data-dropdown]').each(function () {
		if ($(this).hasClass('active')) {
			$(this).parent().addClass('active');
		}
		$(this).click(function () {
			$(this).parent().toggleClass('active');
			$(this).toggleClass('active');
		})
	});

	// menu
	$('[menu-dropdown]').click(function (e) {
		e.stopPropagation();
		document.body.classList.remove('menu-show', 'menu-brand-show');
		$('body').toggleClass(this.getAttribute('data-body-class'));
		return false;
	});

	$(document).click(function () {
		document.body.classList.remove('menu-show', 'menu-brand-show');
	});

	new Tabs();

	(function () {

		var wrapperMobile = $('<div class="wrapper-mobile"></div>');
		$('body').prepend($(wrapperMobile));
		$(wrapperMobile).append($('#js-header__desktop').clone());

	})();

	$('[data-menu-dropdown]').click(function () {
		$(this).parent().toggleClass('active');
	});

	(function () {

		var menuMobile = $('[menu-mobile]');
		var menuBurger = $('[data-burger]');

		$(menuBurger).click(function () {
			$('body').toggleClass('menu-open');
		});

		$(menuMobile).each(function (i) {
			if (i == 0) {
				$(this).addClass('active');
			}
			$(this).click(function () {
				$(menuMobile).removeClass('active');
				$('body').removeClass('menu-mobile-change');
				$(this).addClass('active');

				if (i == 1) {
					$('body').addClass('menu-mobile-change');
				}
			});
		});

	})();

	$('[data-toggle-class]').click(function () {
		$(this).toggleClass('active');
	});

	$('[data-ajax]').click(function (e) {
		e.preventDefault();
		globalPopup.preloader(true);
		fetch(this.dataset.url)
			.then(response => {
				if (!response.ok) {
					globalPopup.preloader(false);
					alert('Error!')
					return;
				}
				response.text().then(data => {
					globalPopup
						.html(data)
						.show()
						.preloader(false)
				});

			});
	});

})();
